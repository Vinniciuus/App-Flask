from flask import Blueprint
from flask import render_template
from models import Post
from flask_security import login_required
from flask import request
from .forms import PostForm
from flask import redirect
from flask import url_for
from models import Post
from app import app, db
app.app_context().push()



posts = Blueprint('posts', __name__, template_folder='templates')





@posts.route('/')
@login_required
def posts_list():
    q = request.args.get('q')
    if q:
        posts = Post.query.filter(Post.title.contains(q) | 
        Post.body.contains(q)).all()
    else:
        posts = Post.query.order_by(Post.created.desc())
    return render_template('posts/posts.html', posts=posts)
    

@posts.route('/<slug>')
@login_required
def post_detail(slug):
    post = Post.query.filter(Post.slug==slug).first()
    return render_template('posts/post_detail.html',  post=post)


@posts.route('/create', methods=['POST', 'GET'])
@login_required
def post_create():
    form = PostForm()
    
    if request.form.get('title'):
        title = request.form.get('title')
        body = request.form.get('body')
        try:
            post = Post(title=title, body=body)
            db.session.add(post)
            db.session.commit()
            return redirect(url_for('posts.post_detail', slug=post.slug))
            
        except:
            print('Very long Traceback')
            return redirect(url_for('posts.post_detail', slug=post.slug))
            
    return render_template('posts/post_create.html', form=form)
    
@posts.route('/<slug>/edit', methods=['POST', 'GET'])
@login_required
def post_update(slug):
    post = Post.query.filter(Post.slug==slug).first()
    
    if request.method == 'POST':
        form = PostForm(formdata=request.form, obj=post)
        form.populate_obj(post)
        db.session.commit()
        return redirect(url_for('posts.post_detail', slug=post.slug))
    
    form = PostForm(obj=post)
    return render_template('posts/edit.html', post=post, form=form)
    
    
